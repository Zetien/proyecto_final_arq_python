from datetime import datetime
from time import sleep

import requests

from arq_python2.celery import app
from movie.models import Currency, RecentMovie
from bs4 import BeautifulSoup


@app.task
def get_recent_movies():
    url = 'https://www.cinecolombia.com/cartagena/cartelera'
    response = requests.get(url)
    
    soup = BeautifulSoup(response.text, 'html.parser')
    cartelera = soup.find('div', attrs={'class': 'is-multiline'})

    title = cartelera.find_all('h2', attrs={'class': 'movie-item__title'})
    title_peliculas = []
    for titulo in title:
        title_peliculas.append(titulo.text)

    images = cartelera.find_all('img')
    images_peliculas = []
    for image in images:
        images_peliculas.append(image.get('data-src'))

    return [title_peliculas, images_peliculas]


@app.task
def save_movie_in_db(results):
    title_peliculas, images_peliculas = results
    RecentMovie.objects.all().delete()
    for index in range (len(title_peliculas)):
        RecentMovie.objects.create(title=title_peliculas[index], img=images_peliculas[index])
                

@app.task
def get_run_chain():
    (get_recent_movies.s() | save_movie_in_db.s())()



