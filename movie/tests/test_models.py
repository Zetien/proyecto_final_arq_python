from django.test import TestCase
from movie.models import RecentMovie


class RecentMovieTest(TestCase):

    def test_create_movie(self):
        count_movie = RecentMovie.objects.count()
        movie_name = 'Movie 1'
        img = "https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
        movie = RecentMovie.objects.create(title=movie_name, img=img)
        self.assertEqual(count_movie + 1, RecentMovie.objects.count())
        self.assertEqual(str(movie), movie_name)
