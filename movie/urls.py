from django.urls import path

from movie.views import MovieListView

app_name = 'movie'

urlpatterns = [
    path('', MovieListView.as_view(), name='recentmovie_list'),
]
