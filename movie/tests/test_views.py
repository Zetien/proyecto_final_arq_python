from django.test import TestCase
from django.urls import reverse_lazy

from movie.models import RecentMovie


class MovieListView(TestCase):

    @classmethod
    def setUpTestData(cls):
        print('cls.setup_test_data_calls')

    def setUp(self):
        print('self.setup_calls')
        RecentMovie.objects.update_or_create(title="MOVIE 1", img="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png")
        RecentMovie.objects.update_or_create(title="MOVIE 2", img="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png")

    def test_1_movie_list_is_empty(self):
        RecentMovie.objects.all().delete()
        url = reverse_lazy('movie:recentmovie_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['recentmovie_list']), 0)
        self.assertContains(response, 'No movies to show')

    def test_test1_case_value(self):
        url = reverse_lazy('movie:recentmovie_list')
        response = self.client.get(url, {'name': 'test1'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['result'], 'this is a test')

    def test_list_is_not_empty(self):
        url = reverse_lazy('movie:recentmovie_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['recentmovie_list']), 2)
