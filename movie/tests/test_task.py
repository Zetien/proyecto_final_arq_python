from django.test import TestCase
from movie.models import RecentMovie
from movie.tasks import *

class TestTask(TestCase):
    def test_get_recent_movies(self):
        recent_movie=get_recent_movies()
        self.assertNotEqual(recent_movie[0], [])
        self.assertNotEqual(recent_movie[1], [])

    def test_save_movie_in_db(self):
        results = get_recent_movies()
        save_movie = save_movie_in_db(results)
        self.assertNotEqual(RecentMovie.objects.count(), 0)
