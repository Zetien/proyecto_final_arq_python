from django.db import models


class Currency(models.Model):
    code = models.CharField(max_length=200)
    value = models.FloatField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.code}: {self.value}' 


class RecentMovie(models.Model):
    title = models.CharField(max_length=255)
    img = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.title}' 
